<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $last=DB::connection('mysql')->select('select f.*,t.name as type_name from food_details f
left join food_type t ON f.food_type = t.id
ORDER BY id DESC LIMIT 1');
//    dd($last[0]);
    $sale=DB::connection('mysql')->select('select * from v_sale');
//    dd($sale);
    $popu= DB::connection('mysql')->select('SELECT s.food_id ,f.name AS food_name,
  f.image,f.price,f.food_type,t.name AS type_name
  ,COUNT(*) AS magnitude
FROM food_rated s
  LEFT JOIN food_details f on s.food_id = f.id
  LEFT JOIN food_type t ON f.food_type = t.id
GROUP BY food_id
ORDER BY magnitude DESC
LIMIT 1');
    return view('welcome',['last'=>$last,'sale'=>$sale,'popu'=>$popu]);
//    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/details','FoodDetailsController');
Route::resource('/search','SearchController');
