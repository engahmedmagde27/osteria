<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewSale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            DB::statement('
create or replace VIEW v_sale as
SELECT s.food_details_id ,f.name AS food_name,
  f.image,f.price,f.food_type,t.name AS type_name
  ,COUNT(*) AS magnitude
FROM food_sales s
  LEFT JOIN food_details f on s.food_details_id = f.id
  LEFT JOIN food_type t ON f.food_type = t.id
GROUP BY food_details_id
ORDER BY magnitude DESC
LIMIT 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("drop view v_sale");
    }
}
