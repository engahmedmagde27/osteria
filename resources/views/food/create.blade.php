<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10-05-2019
 * Time: 1:04 PM
 */?>
@extends('layouts.layout')
@section('content')
    <br>
    <div class="row">
        <div class="col-md-12 profile-dashboard">
            <div class="row">
                <div class="col-md-7 dashboard-form calender">
                    <form class="form-horizontal" action="{{route('details.store')}}" enctype="multipart/form-data" method="post">
                        {{csrf_field()}}
                        <div class="bg-white pinside40 mb30">
                            <!-- Form Name -->
                            <h2 class="form-title">Upload Photo</h2>
                            <div class="form-group">
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="photo-upload"><img src="images/profile-dashbaord.png" alt=""></div>--}}
                                {{--</div>--}}
                                <div class="col-md-8 upload-file">
                                    <input  name="document" class="input-file" type="file" value="">
                                </div>
                            </div>
                            <!-- Text input-->
                            <h2 class="form-title">Food Details</h2>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Name<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input id="name" name="name" type="text" placeholder="Name" class="form-control input-md" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Price </label>
                                <div class="col-md-8">
                                    <input id="name" name="price" type="number" step="any" placeholder="Price" class="form-control input-md" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Food Type<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select id="name" name="food_type" type="text" class="form-control input-md" required="">
                                        <option value="" selected disabled>Select Type</option>
                                        @foreach($food_type as $food)
                                            <option value="{{$food->id}}">{{$food->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Cal<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input id="name" name="cal" type="number" step="any" placeholder="Cal" class="form-control input-md" required="">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="submit"></label>
                                <div class="col-md-4">
                                    <button id="submit" name="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}
    @endsection
