<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10-05-2019
 * Time: 10:01 PM
 */?>
@extends('layouts.layout')
@section('content')
    <div class="tp-page-head">
        <!-- page header -->
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="page-header text-center">
                        <div class="icon-circle">
                            <i class="icon icon-size-60  icon-list icon-white"></i>
                        </div>
                        <h1>Venue Listing No Sidebar / Full page</h1>
                        <p>Venue Listing No Sidebar / Full page for multiple use for vendor.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tp-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Vendor List No Sidebar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="filter-box">
        <div class="container">
            <div class="row filter-form">
                <div class="col-md-12">
                    <h4>Refine Your Search</h4>
                </div>
                <form action="{{route('search.store')}}" enctype="multipart/form-data" class="form-horizontal" method="post">
                    {{csrf_field()}}
                    <div class="col-md-3">
                        <label class="control-label" for="venuetype">Venue Type</label>
                        <select id="venuetype" name="food_type" class="form-control">
                            <option disabled selected>Select Food</option>
                            @foreach(DB::table('food_type')->get() as $n)
                                <option value="{{$n->id}}">{{$n->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label" for="capacity">Capacity</label>
                        <select id="capacity" name="price" class="form-control">
                            <option disabled selected>Select Price</option>
                            <option value="50"> Less 50  L.E</option>
                            <option value="10"> Less 100 L.E</option>
                            <option value="100"> Less 200 L.E</option>
                            <option value="1000"> Less 300 L.E</option>
                            <option value="10000"> Less 400 L.E</option>
                            <option value="10000"> More 400 L.E</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label" for="price">Price</label>
                        <select id="price" name="cal" class="form-control">
                            <option disabled selected>Select Cal.</option>
                            <option value="1"> Less 1 KCal</option>
                            <option value="10"> Less 10 KCal</option>
                            <option value="100"> Less 100 KCal</option>
                            <option value="1000"> Less 1000 KCal</option>
                            <option value="10000"> Less 10000 KCal</option>
                            <option value="10000"> More 10000 KCal</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary btn-block">Refine Your Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mb30">
                        <h2> 12 Food in your search.</h2>
                    </div>
                </div>
                @foreach($result as $item)
                <div class="col-md-4 vendor-box">
                    <!-- venue box start-->
                    <div class="vendor-image">
                        <!-- venue pic -->
                        <a href="#"><img src="{{asset('document/'.$item->image.'')}}" alt="wedding venue" class="img-responsive"
                                         style="max-height: 200px;min-height: 200px;max-width: 400px;min-width: 400px"></a>
                        <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                    </div>
                    <!-- /.venue pic -->
                    <div class="vendor-detail">
                        <!-- venue details -->
                        <div class="caption">
                            <!-- caption -->
                            <h2><a href="#" class="title">{{$item->type_name}}</a></h2>
                            <p class="location"><i class="fa fa-cutlery"></i> {{$item->name}}.</p>
                            <div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(4)</span> </div>
                        </div>
                        <!-- /.caption -->
                        <div class="vendor-price">
                            <div class="price">{{$item->price}} L.E</div>
                        </div>
                        <div class="vendor-price">
                            <div class="price">{{$item->cal}} Cal</div>
                        </div>
                    </div>
                    <!-- venue details -->
                </div>
                @endforeach
                <!-- /.venue box start-->
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-2.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-3.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-4.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-5.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-6.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-7.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-8.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
                {{--<!-- /.venue box start-->--}}
                {{--<div class="col-md-4 vendor-box">--}}
                    {{--<!-- venue box start-->--}}
                    {{--<div class="vendor-image">--}}
                        {{--<!-- venue pic -->--}}
                        {{--<a href="#"><img src="images/vendor-1.jpg" alt="wedding venue" class="img-responsive"></a>--}}
                        {{--<div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>--}}
                    {{--</div>--}}
                    {{--<!-- /.venue pic -->--}}
                    {{--<div class="vendor-detail">--}}
                        {{--<!-- venue details -->--}}
                        {{--<div class="caption">--}}
                            {{--<!-- caption -->--}}
                            {{--<h2><a href="#" class="title">Venue Vendor Title</a></h2>--}}
                            {{--<p class="location"><i class="fa fa-map-marker"></i> Street Address, Name of Town, 12345, Country.</p>--}}
                            {{--<div class="rating "> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(22)</span> </div>--}}
                        {{--</div>--}}
                        {{--<!-- /.caption -->--}}
                        {{--<div class="vendor-price">--}}
                            {{--<div class="price">$390 - $600</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- venue details -->--}}
                {{--</div>--}}
            </div>
            <div class="row">
                <div class="col-md-12 tp-pagination">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous"> <span aria-hidden="true">Previous</span> </a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next"> <span aria-hidden="true">NEXT</span> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        @endsection
