@extends('layouts.layout')
@section('content')
    <div class="slider-bg">
        <!-- slider start-->
        <div id="slider" class="owl-carousel owl-theme slider">
            <div class="item"><img src="{{asset('assets/images/hero-image-3 (1).jpg')}}" alt="Find Your Favorite Food"></div>
            <div class="item"><img src="{{asset('assets/images/header_gourmet_2b.jpg')}}" alt="Find Your Favorite Food"></div>
            <div class="item"><img src="{{asset('assets/images/salad.jpg')}}" alt="Find Your Favorite Food"></div>
        </div>
        <div class="find-section">
            <!-- Find search section-->
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 finder-block">
                        <div class="finder-caption">
                            <h1>Find your Favorite Food</h1>
                            <p>Over <strong>1200+ Food Vendor </strong>for you special date &amp; Find the perfect venue &amp; save you date.</p>
                        </div>
                        <div class="finderform">
                            <form >

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <select class="form-control">
                                            <option disabled selected>Select Food</option>
                                            @foreach(DB::table('food_type')->get() as $n)
                                                <option value="{{$n->id}}">{{$n->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select class="form-control">
                                            <option disabled selected>Select Price</option>
                                            <option value="50"> Less 50  L.E</option>
                                            <option value="10"> Less 100 L.E</option>
                                            <option value="100"> Less 200 L.E</option>
                                            <option value="1000"> Less 300 L.E</option>
                                            <option value="10000"> Less 400 L.E</option>
                                            <option value="10000"> More 400 L.E</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select class="form-control">
                                            <option disabled selected>Select Cal.</option>
                                            <option value="1"> Less 1 KCal</option>
                                            <option value="10"> Less 10 KCal</option>
                                            <option value="100"> Less 100 KCal</option>
                                            <option value="1000"> Less 1000 KCal</option>
                                            <option value="10000"> Less 10000 KCal</option>
                                            <option value="10000"> More 10000 KCal</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">Find Food</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.Find search section-->
    </div>
    <!-- slider end-->
    <div class="section-space80">
        <!-- Feature Blog Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title mb60 text-center">
                        <h1>All Food Between Your Hand</h1>
                        <p>Just Search about you need to eat then enjoy it.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- feature center -->
                <div class="col-md-4">
                    <div class="feature-block feature-center">
                        <!-- feature block -->
                        <div class="feature-icon"><img src="{{asset('assets/images/vendor.svg')}}" alt=""></div>
                        <h2>Find Food Branches</h2>
                        <p>More than 1200 branches between your hand</p>
                    </div>
                </div>
                <!-- /.feature block -->
                <div class="col-md-4">
                    <div class="feature-block feature-center">
                        <!-- feature block -->
                        <div class="feature-icon"><img src="{{asset('assets/images/mail.svg')}}" alt=""></div>
                        <h2>Rate vendor</h2>
                        <p>Don't miss rate your vendor to give users feedback about tasty and vendor.</p>
                    </div>
                </div>
                <!-- /.feature block -->
                <div class="col-md-4">
                    <div class="feature-block feature-center">
                        <!-- feature block -->
                        <div class="feature-icon"><img src="{{asset('assets/images/couple.svg')}}" alt=""></div>
                        <h2>Save Your Moment</h2>
                        <p>You can save every Lovely moment with us .</p>
                    </div>
                </div>
                <!-- /.feature block -->
            </div>
            <!-- /.feature center -->
        </div>
    </div>
    <!-- Feature Blog End -->
    <div class="section-space80 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title mb60 text-center">
                        <h1>Featured , Popular and Top Rated</h1>
                        <p>The part updated every second according to users rating and orders.</p>
                    </div>
                </div>
            </div>
            <div class="row ">
                @if($last != null)
                    <div class="col-md-4">
                        <!-- vendor box start-->
                        <div class="vendor-box">
                            <div class="vendor-image">
                                <!-- vendor pic -->
                                <a href="#"><img src="{{asset('document/'.$last[0]->image.'')}}" alt="wedding vendor" class="img-responsive"
                                                 style="max-height: 200px;min-height: 200px;max-width: 400px;min-width: 400px"></a>
                                <div class="feature-label"></div>
                                <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                            </div>
                            <!-- /.vendor pic -->
                            <div class="vendor-detail">
                                <!-- vendor details -->
                                <div class="caption">
                                    <!-- caption -->
                                    <h2><a href="#" class="title">{{$last[0]->type_name}}</a></h2>
                                    <p class="location"><i class="fa fa-cutlery"></i> {{$last[0]->name}}.</p>
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(2)</span> </div>
                                </div>
                                <!-- /.caption -->
                                <div class="vendor-price">
                                    <div class="price">{{$last[0]->price}} L.E</div>
                                </div>
                            </div>
                            <!-- vendor details -->
                        </div>
                    </div>
                @else
                    <div class="col-md-4">
                        <!-- vendor box start-->
                        <div class="vendor-box">
                            <div class="vendor-image">
                                <!-- vendor pic -->
                                <a href="#"><img src="{{asset('assets/images/vendor-1.jpg')}}"
                                                 style="max-height: 200px;min-height: 200px;max-width: 400px;min-width: 400px"
                                                 alt="wedding vendor" class="img-responsive"></a>
                                <div class="rated-label"></div>
                                <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                            </div>
                            <!-- /.vendor pic -->
                            <div class="vendor-detail">
                                <!-- vendor details -->
                                <div class="caption">
                                    <!-- caption -->
                                    <h2><a href="#" class="title">
                                        </a></h2>
                                    <p class="location"><i class="fa fa-cutlery"></i>.</p>
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(0)</span> </div>
                                </div>
                                <!-- /.caption -->
                                <div class="vendor-price">
                                    <div class="price">0 L.E</div>
                                </div>
                            </div>
                        </div>
                        <!-- vendor details -->
                    </div>
                @endif
            <!-- /.vendor box start-->
                @if($sale != null)
                    <div class="col-md-4">
                        <!-- vendor box start-->
                        <div class="vendor-box">
                            <div class="vendor-image">
                                <!-- vendor pic -->
                                <a href="#"><img src="{{asset('document/'.$sale[0]->image)}}"
                                                 style="max-height: 200px;min-height: 200px;max-width: 400px;min-width: 400px"
                                                 alt="wedding vendor" class="img-responsive"></a>
                                <div class="rated-label"></div>
                                <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                            </div>
                            <!-- /.vendor pic -->
                            <div class="vendor-detail">
                                <!-- vendor details -->
                                <div class="caption">
                                    <!-- caption -->
                                    <h2><a href="#" class="title"> {{$sale[0]->type_name}}</a></h2>
                                    <p class="location"><i class="fa fa-cutlery"></i> {{$sale[0]->food_name}}.</p>
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span> </div>
                                </div>
                                <!-- /.caption -->
                                <div class="vendor-price">
                                    <div class="price">{{$sale[0]->price}} L.E</div>
                                </div>
                            </div>
                        </div>
                        <!-- vendor details -->
                    </div>
                @else
                    <div class="col-md-4">
                        <!-- vendor box start-->
                        <div class="vendor-box">
                            <div class="vendor-image">
                                <!-- vendor pic -->
                                <a href="#"><img src="{{asset('assets/images/vendor-1.jpg')}}"
                                                 style="max-height: 200px;min-height: 200px;max-width: 400px;min-width: 400px"
                                                 alt="wedding vendor" class="img-responsive"></a>
                                <div class="rated-label"></div>
                                <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                            </div>
                            <!-- /.vendor pic -->
                            <div class="vendor-detail">
                                <!-- vendor details -->
                                <div class="caption">
                                    <!-- caption -->
                                    <h2><a href="#" class="title">
                                        </a></h2>
                                    <p class="location"><i class="fa fa-cutlery"></i>.</p>
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(0)</span> </div>
                                </div>
                                <!-- /.caption -->
                                <div class="vendor-price">
                                    <div class="price">0 L.E</div>
                                </div>
                            </div>
                        </div>
                        <!-- vendor details -->
                    </div>
                @endif
            <!-- /.vendor box start-->
                @if($popu != null)
                    <div class="col-md-4 vendor-box">
                        <!-- vendor box start-->
                        <div class="vendor-image">
                            <!-- vendor pic -->
                            <a href="#"><img src="{{asset('document/'.$popu[0]->name)}}" alt="wedding vendor" class="img-responsive"></a>
                            <div class="popular-label"></div>
                            <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                        </div>
                        <!-- /.vendor pic -->
                        <div class="vendor-detail">
                            <!-- vendor details -->
                            <div class="caption">
                                <!-- caption -->
                                <h2><a href="#" class="title">Vendor Photographer Title</a></h2>
                                <p class="location"><i class="fa fa-cutlery"></i> Street Address, Name of Town, 12345, Country.</p>
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(5)</span> </div>
                            </div>
                            <!-- /.caption -->
                            <div class="vendor-price">
                                <div class="price">$390 - $600</div>
                            </div>
                        </div>
                        <!-- vendor details -->
                    </div>
                @else
                    <div class="col-md-4">
                        <!-- vendor box start-->
                        <div class="vendor-box">
                            <div class="vendor-image">
                                <!-- vendor pic -->
                                <a href="#"><img src="{{asset('assets/images/vendor-1.jpg')}}"
                                                 style="max-height: 200px;min-height: 200px;max-width: 400px;min-width: 400px"
                                                 alt="wedding vendor" class="img-responsive"></a>
                                <div class="rated-label"></div>
                                <div class="favourite-bg"><a href="#" class=""><i class="fa fa-heart"></i></a></div>
                            </div>
                            <!-- /.vendor pic -->
                            <div class="vendor-detail">
                                <!-- vendor details -->
                                <div class="caption">
                                    <!-- caption -->
                                    <h2><a href="#" class="title">
                                        </a></h2>
                                    <p class="location"><i class="fa fa-cutlery"></i>.</p>
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(0)</span> </div>
                                </div>
                                <!-- /.caption -->
                                <div class="vendor-price">
                                    <div class="price">0 L.E</div>
                                </div>
                            </div>
                        </div>
                        <!-- vendor details -->
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- /.top location -->

    <!-- /. Testimonial Section -->
    <div class="section-space80">
        <!-- Call to action -->
        <div class="container">
            <div class="row">
                <div class="col-md-6 couple-block">
                    <div class="couple-icon"><img src="{{asset('assets/images/couple.svg')}}" alt=""></div>
                    <h2>Are you user need suggestion place ?</h2>
                    <p>We can suggested you place to enjoy , according to price , cal , location and rate.</p>
                    <a href="#" class="btn btn-primary">Find Place</a> </div>
                <div class="col-md-6 vendor-block">
                    <div class="vendor-icon"><img src="{{asset('assets/images/vendor.svg')}}" alt=""></div>
                    <h2>Are you Food vendor ?</h2>
                    <p>If You food vendor you can advertise with us .</p>
                    <a href="#" class="btn btn-primary">Add Your Items</a></div>
            </div>
        </div>
    </div>
    <!-- /. Call to action -->
@endsection