<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10-05-2019
 * Time: 9:40 AM
 */?>

        <!DOCTYPE html>
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Osteria Food Market</title>
    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Template style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.transitions.css')}}">
    <!-- Font used in template -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:400,400italic,500,500italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
    <!--font awesome icon -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- favicon icon -->
    <link rel="shortcut icon" href="{{asset('assets/images/logo2.png')}}" type="image/x-icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="collapse" id="searcharea">
    <!-- top search -->
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
              <button class="btn btn-primary" type="button">Search</button>
          </span>
    </div>
</div>
<!-- /.top search -->
{{--<div class="top-bar">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 top-message">--}}
                {{--<p>Welcome to Osteria Food.</p>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 top-links">--}}
                {{--<ul class="listnone">--}}
                    {{--@if (Route::has('login'))--}}
                        {{--<li >--}}
                            {{--@auth--}}
                                {{--<a href="{{Auth::logout()}}">Logout</a>--}}
                                {{--<a href="{{route('details.index')}}">Details</a>--}}
                                {{--@else--}}
                                    {{--<li>--}}
                                    {{--<a href="{{ route('login') }}">Login</a>--}}
                                    {{--</li>--}}
                                    {{--@if (Route::has('register'))--}}
                                        {{--<li>--}}
                                        {{--<a href="{{ route('register') }}">Register</a>--}}
                                        {{--</li>--}}
                                            {{--@endif--}}
                                    {{--@endauth--}}
                        {{--</li>--}}
                    {{--@endif--}}
                    {{--<li><a href="login-page.html">Log in</a></li>--}}
                    {{--<li>--}}
                        {{--<a role="button" data-toggle="collapse" href="#searcharea" aria-expanded="false" aria-controls="searcharea"> <i class="fa fa-search"></i> </a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 logo">
                <div class="navbar-brand">
                    <a href="{{ url('/') }}"><img src="{{asset('assets/images/logo2.png')}}" alt="Wedding Vendors" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="navigation" id="navigation">
                    <ul>
                        <li class="active"><a href="{{ url('/') }}">Home</a>
                        </li>
                        @if (Route::has('login'))
                            <li >
                                @auth
                                <li>
                                    <a href="{{route('details.index')}}">Details</a>
                                </li>
                                    <li>
                                    <a href="{{Auth::logout()}}">Logout</a>
                                    </li>

                            <li>

                            </li>
                        @else
                            <li>
                                <a href="{{ route('login') }}">Login</a>
                            </li>
                            @if (Route::has('register'))
                                <li>
                                    <a href="{{ route('register') }}">Register</a>
                                </li>
                                @endif
                                @endauth
                                </li>
                            @endif


                        </li>

                            </ul>

                </div>

            </div>
        </div>
    </div>
</div>
@yield('content')
<div class="footer">
    <!-- Footer -->
    <div class="container">
        <div class="row">
            <div class="col-md-5 ft-aboutus">
                <h2>Osteria Food</h2>
                <p>At Osteria food Market you find what with delicious food . <a href="#">Start Find Food!</a></p>
                <a href="#" class="btn btn-default">Find a Food</a> </div>
            <div class="col-md-3 ft-link">
                <h2>Useful links</h2>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact us</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Career</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Use</a></li>
                </ul>
            </div>
            <div class="col-md-4 newsletter">
                <h2>Subscribe for Newsletter</h2>
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Enter E-Mail Address" required>
                        <span class="input-group-btn">
            <button class="btn btn-default" type="button">Submit</button>
            </span> </div>
                    <!-- /input-group -->
                    <!-- /.col-lg-6 -->
                </form>
                <div class="social-icon">
                    <h2>Be Social &amp; Stay Connected</h2>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Footer -->
<div class="tiny-footer">
    <!-- Tiny footer -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">Copyright © 2019. All Rights Reserved</div>
        </div>
    </div>
</div>

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Flex Nav Script -->
<script src="{{asset('assets/js/jquery.flexnav.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/navigation.js')}}"></script>
<!-- slider -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/slider.js')}}"></script>
<!-- testimonial -->
<script type="text/javascript" src="{{asset('assets/js/testimonial.js')}}"></script>
<!-- sticky header -->
<script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
<script src="{{asset('assets/js/header-sticky.js')}}"></script>
</body>

</html>
