<?php

namespace App\Http\Controllers;

use App\FoodDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FoodDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUpload($document)
    {
        try{
            $input['document'] = time().'_'.$document->getClientOriginalName();

            $destinationPath = public_path('/document');

            $document->move($destinationPath, $input['document']);


            //$this->postImage->add($input);

            return $input['document'];
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function index()
    {
        $food_type=DB::table('food_type')->get();
        return view('food.create',['food_type'=>$food_type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd(Auth::user());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $this->validate($request,[
            'name'=>'required',
            'price'=>'required',
            'cal'=>'required',
            'food_type'=>'required',
        ],[
            'name.required'=>' Name is required.',
            'price.required'=>'price is required',
            'cal.required'=>'cal is required.',
            'food_type.required'=>'Type is required',
//            'logo.required'=>'logo is required.',
        ]);
        if ($request->document != null) {

            $documents = $request->file('document');
            $document = $this->fileUpload($documents);
            $request['image'] = $document;

//            dd($request->input());

        }
//        $image = $request->file('logo');
        if(FoodDetails::create($request->input())){
//            $request['logo'] = $image_name;
//            $company = Company::create($request->input());
            return redirect()
                ->action('FoodDetailsController@index')
                ->with(['success'=>"Items Created Successfully."]);
        }else{
            return redirect()
                ->action('FoodDetailsController@index')
                ->with(['Failed'=>"Company Created Failed."]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
