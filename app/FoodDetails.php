<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodDetails extends Model
{
    protected $table = 'food_details';
    protected $fillable = ['name', 'price','cal','image','food_type'];

}
